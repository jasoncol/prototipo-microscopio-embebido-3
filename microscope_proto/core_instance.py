#main
from core import CoreVision 
config_file  = './config_stable.cfg'

def start():
    return CoreVision(config_file)

def stop(cvis):
    print "Stop Core"
    #stop camera
    if cvis.has_camera:
        cvis.acq.stop()
    #stop serial
    if cvis.has_serial:
        cvis.sm.__del__()  #detener hilo de recepcion de datos serial

cvis = start()
