# -*- coding: utf-8 -*-
import numpy as np
from numpy.fft  import fft2, fftshift, ifft2
import cv2
#import matplotlib.pyplot as plt
import time
#import pdb

def holo_convacel(im, zi=0.1, zf=0.1, step=0.005):  #0.005
    ''' Implementación algoritmo de reconstrucción de holograma por convolución estandar con método acelerado. Basado en ConvolucionParticulas2.m '''
    imd = im.astype(np.float64)
    m, n = im.shape
    i = 0 + 1j
    deltaX = 9 * 10 ** -6
    deltaY = 9 * 10 ** -6
    contar = 10 #Distancia de registro y reconstrucción
    lambda_ = 632 * 10 ** -9 #Longitud de onda
    
    start,  end = zi, zf
    ddv = np.arange(start, end, step) if end > start else (start, )

    g0 = np.zeros(imd.shape, np.complex)
    #IMP1
    '''
    #Implementación 1 usando FORS anidados. Numpy es muy lento usando esta aproximación. Es necesario usar operaciones matriciales.

    
    for dd in ddv:
        for fila in xrange(m):
            for columna in xrange(n):
                fila2 = (fila + 1) - (m / 2.)
                columna2 = (columna + 1) - (n / 2.)
                g0[fila, columna] = (1 / (i * lambda_)) * np.exp((i * 2 * np.pi / lambda_) * np.sqrt(dd ** 2 + ((fila2) ** 2) * deltaX ** 2 + ((columna2) **2) * deltaY ** 2)) / np.sqrt(dd ** 2 + ((fila2) ** 2) * (deltaX ** 2) + ((columna2) ** 2) * deltaY ** 2)
    '''
    #IMP2
    #USANDO operaciones matriciales
    #dd = 0.1
    fila = np.linspace(0, m-1, m) + 1. - (m / 2.)
    columna = np.linspace(0, n - 1, n) + 1. - (n / 2.)
    columna2, fila2 = np.meshgrid(columna, fila)

    for dd in ddv:
        print "iteracion ", dd
        g0 = (1 / (i * lambda_)) * np.exp((i * 2 * np.pi / lambda_) * np.sqrt(dd ** 2 + ((fila2) ** 2) * deltaX ** 2 + ((columna2) **2) * deltaY ** 2)) / np.sqrt(dd ** 2 + ((fila2) ** 2) * (deltaX ** 2) + ((columna2) ** 2) * deltaY ** 2)
        
        #FFT
        g00 = fft2(g0)
        obj = fft2(imd)
        
        resulta = obj * g00
        resulta2 = fftshift(ifft2(resulta))
        rea = abs(resulta2)
        '''
        ig = plt.imshow(abs(rea).astype(np.float32))
        ig.set_cmap('gray')
        plt.show()
        '''
        contar += 1
    
    #MATRIZ PROPAGACION
    matriz_progagacion = (-i * np.pi * lambda_) * (((fila2 / (m * deltaX)) ** 2) +  ((columna2 / (n * deltaY)) ** 2))
    
    
    #Ult calculo
    zprima = 0.10;
    gnueva = np.exp(zprima * matriz_progagacion)
    gnueva00 = gnueva
    obj2 = obj # es el mismo obj
    resulta10 = fftshift(obj2) * gnueva00
    resulta20 = ifft2(resulta10)

    '''
    ig = plt.imshow(abs(rea).astype(np.float32))
    ig.set_cmap('gray')
    plt.show()
    '''
    rea = abs(resulta20).astype(np.float32) # Parte real
    imprint = (rea * 255./rea.max()).astype(np.uint8) #Escalado a 8 bits
    return rea, imprint

def holo_juptner(im, zi=71, zf=71, step=1):
    ''' Recostrucción del holograma basado en el método de Juptner usando transformada de Fesnel. Basado en RecuAprox_01.m '''
    kt = time.time()
    lambda_ = 0.0006328
    dist = 75.0
    distph = 19.1
    sizeph = 0.025
    sample = 0.001
    sampleccd = 0.009
    window = 400
    window2 = 400
    k = 2 * np.pi / lambda_
    z = distph + dist

    #Spherical samples of  5 um
    holoaux = im.astype(np.float64)
    cy, cx = int(im.shape[0] / 2), int(im.shape[1] / 2)
    holo2 = holoaux[round(cy - window / 2):round(cy + window/2 - 0) ,\
            round(cx - window/2): round(cx + window/2 - 0) ]
    holo3 = centrado_mat(np.zeros((window2, window2), np.float64), holo2, 'fusion')

    factor = sampleccd / sample
    holo = cv2.resize(holo3, (int(holo3.shape[0]*factor), int(holo3.shape[1]*factor)), interpolation=cv2.INTER_NEAREST)#NEAREST interp


    #Spherical wave phase
    space = len(holo[0])
    limite = sample * space / 2
    print "len", len(np.arange(-limite, limite-sample + sample , sample))
    n1 = np.arange(-limite, limite-sample + sample , sample)
    n2 = np.arange(-limite, limite-sample + sample, sample)
    x, y = np.meshgrid(n1,n2)
    
    phi, r = cart2polar(x, y)
    sphere_f = np.exp(1j * k *z) * np.exp(1j *k *r ** 2 / 2 / z)
    #Discrete Fresnel equation
    #step = 1
    fromp = zi # Initial distance from Hologram plane
    top = zf # Final distance from hologram plane
    angle = 0.0
    rm = cv2.getRotationMatrix2D((cx, cy), angle, 1.)

    for jump in np.arange(fromp, top + step, step):
        dist2 = jump
        if (sample ** 2 * space / lambda_ / dist2) > 1:
            print "The phase is not being resolved in the spherical term"
            print "Reduce matrix size (space). reduce sample size (sample)"
            print "Increase wavelength (lambda), or increase the propagation distance (dist)"
        fr_sphere = np.exp(-1j * np.pi / lambda_ / dist2 * r ** 2)
        input_ = holo * sphere_f * fr_sphere
        sample_p = fftshift(ifft2(input_))
        isample_p = abs(sample_p * np.conj(sample_p)).astype(np.float64)
        rm = cv2.getRotationMatrix2D((isample_p.shape[1] / 2, isample_p.shape[0] / 2), angle, 1.)
        isample_p = cv2.warpAffine(isample_p, rm, (isample_p.shape[1], isample_p.shape[0]))

        zoom = round(space * (z - jump) / z / (jump ** .6))
        print "zoom", zoom
        imprint = centrado_mat(isample_p, np.zeros((zoom, zoom), np.float64), 'tajada')
        print imprint.max(), imprint.min(), imprint.mean()

        imprint_u8 = (imprint * 255./imprint.max()).astype(np.uint8)

        return imprint, imprint_u8

def centrado_mat(m1, m2, metodo='fusion'):
    ''' Centro de matriz M2 en la matriz M1. Basado en centradomat.m '''

    m, n = m1.shape
    mm, nn = m2.shape
    mat = m1.copy()

    if metodo == 'fusion':
        mat[round(m/2-mm/2) + 0:round(m/2+mm/2), round(n/2-nn/2)+0:round(n/2+nn/2)] = m2
    elif metodo == 'tajada':
        mat = m1[round(m/2-mm/2) + 1:round(m/2+mm/2), round(n/2-nn/2)+1:round(n/2+nn/2)]
    else:
        raise Exception #undefined method

    return mat

def cart2polar(x, y):
        r = np.sqrt(x**2 + y**2)
        theta = np.arctan2(y, x)
        return  theta, r

def holo_angular(im, inicial=60, final=61, step=1): 
    '''Espectro angular'''
    if im.shape[1] % 2 == 0:
        im = im[:, :-1]

    if im.shape[0] % 2 == 0:
        im = im[:-1, :]
   
    XRGB = im #gray image
    X = XRGB
    lambda_=0.0006328; # en mm
    dx0=0.009; # mm ancho de pixel
    L0=X.shape[0]*dx0; #shape 0 or 1?
    k=2*np.pi/lambda_;
    #[M,N]=size(X);
    M, N = im.shape
    X=X.astype(np.float32)
    K=max(M,N);
 
    li = 0 + 1j
    #Zeros-padding to get KxK image
    print "Zero padding, K", K
    try:
        Z1=np.zeros((K,(K-N)/2), np.float32);
        Z2=np.zeros(((K-M)/2,N), np.float32);
        #Xp=[Z1,[Z2;X;Z2],Z1]; #TODO revisar
        print "z1 shape", Z1.shape
        print "z2 shape", Z2.shape

        Xp = np.concatenate((Z2,X, Z2), axis=0)
        Xp = np.concatenate((Z1, Xp, Z1), axis=1)
    except Exception, e:
        print "Warning: Could not pad, error: ", e
        Xp = X

    ''' # ALternate
    m_, n_ = cv2.getOptimalDFTSize(M), cv2.getOptimalDFTSize(N)
    pad = cv2.copyMakeBorder(im, 0, m_ - M, 0, n_ - N, borderType=cv2.BORDER_CONSTANT, value=0)
    Xp = pad.astype(np.float32)
    '''
    print "Xp shape", Xp.shape

    zmax=L0**2/K/lambda_;
    U0=Xp;
     
    #inicial=zi; #TODO params?
    #final=zf;
 
    CC=np.zeros((1,(final-inicial+1)));

    #All this block was extracted from the FOR  below to avoid unnecessary recalculations
    Uf=np.fft.fft2(U0); #TODO zero padded
    Uf0 = Uf
    Uf0_shift=np.fft.fftshift(Uf0); #TODO % Spectrum of the initial field
    fex=K/L0;
    fey=fex;#% sampling of frequency plane
    #MESHGRID
    #fx=[-fex/2:fex/K:fex/2-fex/K];
    fx = np.arange(-fex / 2, fex / 2 -fex/K , fex / K)
    if max(fx.shape) < K:
        fx = np.arange(-fex / 2, fex / 2  , fex / K)

    #fy=[-fey/2:fey/K:fey/2-fey/K];
    fy = np.arange(-fey/2, fey/2 -fex/K, fey/K)
    if max(fy.shape) < K:
        fy = np.arange(-fey/2, fey/2 , fey/K)

    FX,FY=np.meshgrid(fx,fy)
    nsqrt = np.sqrt(1-(lambda_* FX)**2 -(lambda_*FY)**2)
    #inicial, final= 62, 75
    #step = 1
    final = final + step if inicial == final else final
    for  j in np.arange(inicial, final, step):
        z0=j
        print "iteracion ", j
        #Uf=np.fft.fftshift(Uf0); #TODO % Spectrum of the initial field. Put outside FOR
        Uf = Uf0_shift
        #fex=K/L0; #TODO Put outside FOR to avoid recalculations
        #fey=fex;#% sampling of frequency plane
        #fx=[-fex/2:fex/K:fex/2-fex/K];
        #fx = np.arange(-fex / 2, fex / 2 , fex / K)
        #fy=[-fey/2:fey/K:fey/2-fey/K];
        #fy = np.arange(-fey/2, fey/2 , fey/K)
        #FX,FY=np.meshgrid(fx,fy)
        #nsqrt = np.sqrt(1-(lambda_* FX)**2 -(lambda_*FY)**2)
        
        G = np.exp(li * k * z0 * nsqrt)#Angular spectrum transfer function
        #% Diffraction
        #pdb.set_trace()
        result= Uf*G;
        Uf=np.fft.ifft2(result); #TODO zero padded (K, K)
        If2=np.abs(Uf);
 
		#% 8-bit digitization
		#%Imax=max(max(If)); 
		#%If2=uint8(255*If/Imax);
		#%figure,imshow(If2)
     
		#%XY=ginput(2);
 
		#%If2 = double(imcrop(If2, [237 249  128 128]));
        ph,qh=If2.shape
 
		#%nom='Reconstruccion_60mm_lim2.tif';
		#%imwrite(If2,nom);% Recording the hologram
 		#TODO esto es correlacion parece, implementar?. la propagacion sería lo inicial y la ejecucion de lo que esta en este FOR hasta aqui.
		#% Coeficiente de Correlacion
		#%corrcoef
        '''
        if j==inicial:   #%inicial 
			FM=If2.mean();
			Fmn=If2;
			#save Fm Fmn TODO  wtf?
        else:
			GM=If2.mean();  
			Gmn=If2;
			#%j
			CCnum=0;
			CCden1=0;
			CCden2=0; 
 
			for m in range(1, ph + 1):
				for n in range(1, qh + 1):
				
				CCnum=CCnum+(Fmn[m,n]-FM)*(Gmn[m,n]-GM);
				CCden1=CCden1+(Fmn[m,n]-FM)^2;
				CCden2=CCden2+(Gmn[m,n]-GM)^2;

			CC[j-inicial+1]=CCnum/np.sqrt(CCden1*CCden2);
        '''
        #figure(2), plot(j,CC(j-inicial+1),'r*')
        #hold on; %  axis equal;axis tight;
        #xlabel('Coeficiente de Correlacion');
        #%pause
         
        #%---- fin de Coefcorr
        #%%
        #%figure(2),imagesc(abs(Uf)),colormap(gray);ylabel('pixels');
        #%axis equal;axis tight;
        #%label(['Diffraction distance = ',num2str(z0),' mm, Width of the observation plane= ',num2str(L0),'mm']);title('Amplitude of field diffracted by D-FFT');
        #%ylabel(['Diffraction distance: ',num2str(z0),'mm'])
        # 
        #%pause
	#% 8-bit digitization
    #%Imax=max(max(If)); 
    #%If2=uint8(255*If/Imax);
    #%figure,imshow(If2)
    If_u8 = (If2 * 255 / If2.max()).astype(np.uint8) #cast as unsigned int8 for visualization

    return If2, If_u8
     

if __name__ == '__main__':
    im = cv2.imread('diskimg/Montaje1Frances.png', 0)
    k = time.time()
    im = cv2.resize(im, (640, 480))
    a, b = holo_convacel(im)
    cv2.imshow("test", b)
    cv2.waitKey(0)
    print "ET", time.time() - k
