import cv2
import time
import numpy as np
def et(k, msg):
   # print "ET: ", time.time() - k, msg
   return
def pad_fourier(im):
    mm, nn = im.shape
    m = cv2.getOptimalDFTSize(mm)
    n = cv2.getOptimalDFTSize(nn)
    pad = cv2.copyMakeBorder(im, 0, m - mm, 0, n - nn, borderType=cv2.BORDER_CONSTANT, value=0)
    return pad

def cv_dft(im):
    ''' Computes and displays DFT magnitude of image im '''
    k = time.time()
    et(k, "started")
    #get padding values to maake the image square and optimal for DFT
    mm, nn = im.shape
    m = cv2.getOptimalDFTSize(mm)
    n = cv2.getOptimalDFTSize(nn)
    et(k, "OptimalDFTSize")
    #pad image with values
    pad = cv2.copyMakeBorder(im, 0, m - mm, 0, n - nn, borderType=cv2.BORDER_CONSTANT, value=0)
    et(k, "Image padded")
    phase = np.zeros(pad.shape, np.float64) #to hold phase
    expanded = cv2.merge([pad.astype(np.float64), phase]) #create an array of arrays "A" where A[0] holds the mag and A[1] the phase
    et(k, "phase and expanded image")
    res = cv2.dft(expanded) #dft
    et(k, "DFT applied")
    spl0, spl1 = cv2.split(res) #split DFT(A) 
    et(k, "result splitting")
    #spl = res[:,:, 0], res[:,:, 1]
    mag = cv2.magnitude(spl0, spl1) #Compute DFT magnitude
    et(k,"image magnitude")
    #Data normalization and processing for visualization purpose
    mag += 1.0
    mag = cv2.log(mag)
    mag = cv2.normalize(mag, alpha=0, beta=1, norm_type=cv2.NORM_MINMAX)
    et(k,"Normalization and postprocessing")
    #Quadrant swapping for visualization
    cy, cx = mag.shape[0] / 2 , mag.shape[1] / 2
    tmp = mag[0:cy, 0:cx].copy()
    mag[0:cy, 0:cx] = mag[cy:cy*2, cx:cx*2]
    mag[cy:cy*2, cx:cx*2] = tmp
    tmp = mag[0:cy, cx:cx*2].copy()
    mag[0:cy, cx:cx*2] = mag[cy:cy*2, 0:cx]
    mag[cy:cy*2, 0:cx] = tmp
    et(k, "Quadrant correction for visualization")
    cv2.imwrite("test.png", mag)
    return res, mag


if __name__ == '__main__':

    im = cv2.imread('m2.png', 0)
    im = cv2.resize(im, (640, 480))
    print "Image shape", im.shape
    print "Number of pixels", im.shape[0] * im.shape[1]
    k = time.time()
    dft_res, dft_mag = cv_dft(im)
    print "elap", time.time() - k
    #Visualization
    #cv2.imshow("test", dft_mag)
    #cv2.waitKey(0)
