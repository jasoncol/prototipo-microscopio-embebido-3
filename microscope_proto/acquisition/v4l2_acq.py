import ueye
from adq import CameraAcquisition
import webcam_drv as drv
from cStringIO import StringIO
import Image
import numpy as np

class V4L2Acquisition(CameraAcquisition):
    
    def __init__(self, *args, **kwargs):
        super(V4L2Acquisition, self).__init__(*args,**kwargs)
        self.last_frame = None
        drv.init_grab()

    def start(self):
        pass

    def grab(self, retry=False): 
        print "grabbing"
        frm = None
        try:
            
            buf = drv.grab_image()
            file_jpgdata = StringIO(buf)
            frm = np.asarray(Image.open(file_jpgdata))

            if(frm == None and retry):
                print "couldn't grab. Retrying"
                self.grab(retry) #If retry 
            self.last_frame = frm
        except Exception, e:
            print "Error grabbing frame"
            print e
            if retry:
                return self.grab(retry)

        return frm

    def stop(self):
        drv.close()
