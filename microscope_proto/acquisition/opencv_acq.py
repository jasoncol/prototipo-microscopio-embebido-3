import cv2
import time
from adq import CameraAcquisition

class OpencvAcquisition(CameraAcquisition):

    def __init__(self, *args, **kwargs):
        super(OpencvAcquisition, self).__init__(*args, **kwargs)
        #prove devices until something is found
        d = 0
        v = cv2.VideoCapture(0)
        while not v.isOpened() and d < 10:
            d += 1
            v = cv2.VideoCapture(d)
        if v.isOpened():
            print "OPENCV: Found device at", d
        else:
            print "OPENCV: No device was found"
        
        self.vc = v

    def grab(self):
        frm = None
        try:
            print "grabbing opencv", time.time()
            g = self.vc.grab()
            if g:
                frm = self.vc.retrieve()[1]
        except:
            print "error grabbing"
        return frm

    def close(self):
        pass#TODO
