import cv2
import time
from adq import CameraAcquisition
import os
 
class DiskAcquisition(CameraAcquisition):
    def __init__(self, *args, **kwargs):
        self.path = kwargs['path']
        print "Disk image path", self.path
        self.imgs = [x for x  in os.listdir(self.path) if '.png' in x]
        self.cnt = 0
    def grab(self):
        im = None
        try:
            print "Grabbing from disk", self.imgs
            im = cv2.imread(self.path +'/' + self.imgs[self.cnt], 0)
            self.cnt += 1

            if (self.cnt == len(self.imgs)):
				self.cnt = 0
        except:
			print "error loading file"
        return im

	def close(self):
		return True
