import ueye
from adq import CameraAcquisition


class ThorlabsAcquisition(CameraAcquisition):
    
    def __init__(self, *args, **kwargs):
        super(ThorlabsAcquisition, self).__init__(*args,**kwargs)
        print "creating camera"
        self.cam = ueye.Cam()
        print "configuring"
        ueye.SetErrorReport(False)
        self.cam.SetPixelClock(5)#9
        self.cam.SetFrameRate(15)#5
        print "done"
        try:
            self.cam.SetAutoParameter(ueye.SET_ENABLE_AUTO_GAIN, 0, 0)
            self.cam.SetAutoParameter(ueye.SET_ENABLE_AUTO_GAIN, 0, 0)
            self.cam.SetAutoParameter(ueye.SET_ENABLE_AUTO_WHITEBALANCE, 0, 0)
            #self.cam.SetAutoParameter(ueye.SET_ENABLE_AUTO_SENSOR_GAIN, 0, 0)
            #self.cam.SetColorMode(ueye.CM_MONO8)
            self.cam.SetExposureTime(147)
            #self.cam.SetExposureTime(983)
            print "exp range",self.cam.GetExposureRange()
        except Exception, e:
            print "Error setting thorlabs param", e
            pass

        self.last_frame = None

    def start(self):
        pass

    def grab(self, retry=False): 
        print "grabbing"
        frm = None
        try:
            
            frm = self.cam.GrabImage(Timeout=1500, LeaveLocked=False)
            if(frm == None and retry):
                print "couldn't grab. Retrying"
                self.grab(retry) #If retry 
            self.last_frame = frm
        except Exception, e:
            print "Error grabbing frame"
            print e
            if retry:
                return self.grab(retry)

        return frm

    def stop(self):
        pass
