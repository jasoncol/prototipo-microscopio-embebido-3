import struct
import time
from uart import SerialUART
from threading import Thread

class SerialMessageManager(object):
    ''' Controla la comunicacion (envio y recepcion) de informacion usando el 
    puerto serial '''

    HEADER_DATA = "tic"
    FOOTER_DATA = "tfc"
    END, ENDR = '\n', '\r'
    SEP = ','
    LIMIT = 100
    RHEADER_DATA = "mic"
    RFOOTER_DATA = "mfc"

    #COMMANDS TO SEND
    SEND_COM_CHECK_HARDWARE = 1
    SEND_COM_SETLED = 2
    SEND_COM_SETPIEZO = 3
    SEND_COM_TURNOFF = 5
    #SEND_COM_SETPIEZO2 = 8
    #COMMANDS TO RECEIVE
    RECV_COM_CHECK_HARDWARE = 1
    RECV_COM_OK = 7
    RECV_COM_GRAB = 5

    def __init__(self, pserial=None, sendbin=True, *args, **kwargs):
        self.pserial = pserial
        self.sendbin = sendbin
        self.listen_interval = 1. / 20.
    
    def build_command(self, command, values=''):
        '''
        Build command to send to the serial port. 'values' can be an empty string, a value (number or string), and a list/tuple of values.
        returns: message, checksum
        '''

        if isinstance(values, tuple) or isinstance(values, list):
            values = self.SEP.join([str(x) for x in values]) #join all elements in list
        
        if values != '':
            data = self.SEP.join([self.HEADER_DATA, str(command), str(values),self.FOOTER_DATA, self.END]) 
        else:
            data = self.SEP.join([self.HEADER_DATA, str(command), self.FOOTER_DATA, self.END]) 

        chksum = self.generate_chksum(data)
        data += chksum + self.ENDR

        return data, chksum

    def print_data(self, data):
        print repr(data)

    def send_command(self, command, values=''):
        '''
        Send command to send to the serial port. 'values' can be an empty string, a value (number or string), and a list/tuple of values.
        returns: message, checksum
        '''
        data, chksum = self.build_command(command, values)
        data2send = data
        if self.sendbin:
            data2send = struct.pack('%sc'%(len(data)), *data)

        #TODO data limits
        try:
            self.pserial.write(data2send, end='')
        except:
            print "Error sending data to serial"


    def data2list(self, data):
        dl = data.split(SerialMessageManager.SEP)
        outl = []
        for i, s in enumerate(dl):
            if s == SerialMessageManager.FOOTER_DATA:
                break
            if i >= 1:
                outl.append(s)

        return outl
    
    
    def start_listening(self, cback=None):
        print "Starting daemon to receive data"
        self.run_t = True
        self.cback = cback
        self.tf = Thread(target=self._recv_data)
        self.tf.daemon = True
        self.tf.start()

    
    def _recv_data(self):
        out = ''
        while self.run_t:
              try:
                  while self.pserial.p.inWaiting() > 0:#read buffer until is empty
                      out += self.pserial.read(self.pserial.p.inWaiting())
              except Exception, e:
                  print "Error reading buffer"
                  print e
              if out is None:
                  #print "got NULL", out
                  continue
              #check if the message is complete (contains ENDR)
              if SerialMessageManager.ENDR in out:
                  if self.validate_chksum(out):
                      self.last_output = out

                      if self.cback is not None:
                          self.cback(self.data2list(out))
                  else:
                      out = ''
              else:
                  #print "data incomplete. Continue listening"
                  pass
                  #TODO should we reset?

              time.sleep(self.listen_interval) #Read buffer 20 times per second


    def generate_chksum(self, data):
        cval = 0
        for c in [ord(x) for x in data]:#TODO do we need unicode?
            cval ^= c
            
        return str(255 - cval)

    def validate_chksum(self, data):
        #extract chksum
        chksum = data[data.index(SerialMessageManager.END) + 1 : data.index(SerialMessageManager.ENDR)]
        d = data[:data.index(SerialMessageManager.END) + 1]
        c = self.generate_chksum(d)

        if chksum == c:
            return True
        else:
            return False
    
    def __del__(self):
        print "Closing port connection"
        self.run_t = False
        if self.pserial.p.isOpen():
           self.pserial.close()

if __name__ == '__main__':
    p = SerialUART()
    try:
        p.open('/dev/ttyO4')
    except:
        pass

    s = SerialMessageManager(p, sendbin=False)
    s.start_listening()
    #s.send_command(SerialMessageManager.COMMAND_SET_PWM, 1000)
    for i in range(5):
        s.send_command(SerialMessageManager.SEND_COM_SETPIEZO, 1)
        time.sleep(1)
    exit()
    while True: #Main thread simmulation
        try:
            val =int(raw_input('> Insert value to send: '))
            s.send_command(SerialMessageManager.COMMAND_SET_PWM, val)
            time.sleep(1)
        except KeyboardInterrupt:
            print "error k"
            s.__del__()
            break
        except Exception:
            print "error"
            s.__del__()
            break
