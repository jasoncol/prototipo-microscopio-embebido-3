import serial
'''
http://pyserial.sourceforge.net/pyserial_api.html
'''

def open_uart(dev):
    p = serial.Serial(port=dev, timeout=1, baudrate=19200)
    '''
    #other options
    baudrate=9600,
    parity=serial.PARITY_ODD,
    stopbits=serial.STOPBITS_TWO,
    bytesize=serial.SEVENBITS
    '''
    p.open()
    print "opening",dev, ": ", p.isOpen()
    return p


def close_uart(p):
    p.close()


def write_uart(p, msg, end=''):
    print "UART Writing:", repr(msg + end), "to", p.portstr
    p.write(msg + end)


def read_uart(p, size=1):
    s = p.read(size)
    return s

class SerialUART(object):

    def open(self, dev):
        self.p = open_uart(dev)
        if self.p.isOpen():
            self.p.flushInput()
            self.p.flushOutput()
        
        return self.p
       
    def close(self):
        close_uart(self.p)

    def write(self, msg, end='\r\n'):
        write_uart(self.p, msg, end)

    def read(self, size=1):
        return read_uart(self.p, size)

        

if __name__ == '__main__':
    p = open_uart('/dev/ttyO4')
    write_uart(p, 'Hola Mundo')
    s = read_uart(p, 1)
    print "Readed", s
    close_uart(p)
