'''
Movimiento de piezo y adquisicion  de imagen
'''
from acquisition.thorlabs import ThorlabsAcquisition
from pserial.serial_messages import SerialMessageManager
from pserial.uart import SerialUART
import cv2
import time

def version1(sm, su, thor_adq): #version AR
    cnt = 0
    print "Sending Piezo signal", piezo_voltage
    sm.send_command(SerialMessageManager.COMMAND_SET_PIEZO, piezo_voltage)
    print "Waiting for movement to complete"
    time.sleep(0.51)

    while True:
        cnt += 1
        print "iter ",cnt
        k = time.time()
        print "Grabbing frame"
        frm = thor_adq.grab(True)
        print "saving frame"
        #cv2.imwrite(out_folder + "%s.bmp"%(cnt), frm)
        kf = time.time() - k
        print "Waiting"
        time.sleep(max((2.0 - kf), 0))


def version2(sm, su, thor_adq): #version J
    ''' Version utilizada para exp superresolucion '''
    disps = [0.1, 2, 3, 5, 10, 400]
    num_imgs = [400, 85, 56, 34, 17, 4]
    val = 0
    cnt = 0
    session = 1
    k = time.time()
    print "--STARTING EXPERIMENT --"
    print ""
    out_folder = "out_piezo/"

    sm.send_command(SerialMessageManager.SEND_COM_SETLED, 1)

    for i, d in enumerate(disps):
        session = i + 1
        print "SESSION: ", session, "Voltage:", d
        for c in range(num_imgs[i]):
            print "ITER: ", c 
            print "Sending Move Piezo Signal.", time.time() - k
            sm.send_command(SerialMessageManager.SEND_COM_SETPIEZO, session)
            print "Waiting for movement to complete.", time.time() -k 
            time.sleep(0.52)
            print "Grabbing frame", time.time() - k 
            frm = thor_adq.grab()
            print "Saving frame", time.time() - k
            cv2.imwrite(out_folder + "s%s/%s.png"%(session,c + 1), frm)
            #print "Waiting"
            time.sleep(0.1)
        print "TURN OFF"
        sm.send_command(SerialMessageManager.SEND_COM_TURNOFF)
        time.sleep(5)
    print "End"

def intensidades(sm, su, thor_adq):
    salto = 1
    inicio = 0
    fin = 300
    val = inicio
    out_folder = 'out_piezo/led_int/'
    sm.send_command(SerialMessageManager.SEND_COM_SETLED, 0)
    return
    print "INIT INTENSIDADES LED"
    while val <= fin:
        print "-- Intensity", val
        sm.send_command(SerialMessageManager.SEND_COM_SETLED, val)
        val += salto
        time.sleep(0.5)
        print "grabbing frame"
        frm = thor_adq.grab()
        print "saving frame"
        cv2.imwrite(out_folder + "%s.png"%(val), frm)
        time.sleep(0.1)
    print "END"

if __name__ == '__main__':
    uart_dev = '/dev/ttyO4'
    piezo_voltage = 0.1
    out_folder = 'out_piezo/%s/'%(1)

    try: 
        su = SerialUART()
        su.open('/dev/ttyO4')
        sm = SerialMessageManager(pserial=su, sendbin=False) 
        #sm.send_command(SerialMessageManager.SEND_COM_SETLED, 500)
        thor_adq = ThorlabsAcquisition()
    except Exception, e:
        print "Error on initialization"
        print e
        su.close()
        exit()
   
   #MAIN THREAD OF EXECUTION
    try:
        version2(sm, su, thor_adq)
        #intensidades(sm, su, thor_adq)
        
    except Exception, e:
       print "Error on execution"
       print e
    
    su.close()
