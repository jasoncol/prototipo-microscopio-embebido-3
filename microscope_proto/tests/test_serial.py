from pserial.serial_messages import SerialMessageManager
from pserial.uart import SerialUART
import cv2
import time

'''
test de piezo e intensidades LED
'''

#ACTIVAR uart en beagle con script de inicializacion
uart_dev = '/dev/ttyO4'
try: 
    print "INITIALIZING SERIAL"
    su = SerialUART()
    su.open('/dev/ttyO4')
    sm = SerialMessageManager(pserial=su, sendbin=False) 
except Exception, e:
    print e
    su.close()
    exit()

print "TESTS"

#TESTS
print "LED TEST"
#LED OFF
sm.send_command(SerialMessageManager.SEND_COM_SETLED, 0)
time.sleep(2)
t = 0
while t < 100:
    #LED ON
    sm.send_command(SerialMessageManager.SEND_COM_SETLED, 500)
    time.sleep(2)
    #LED OFF
    sm.send_command(SerialMessageManager.SEND_COM_SETLED, 0)
    time.sleep(2)
    t += 1

time.sleep(2)
#PIEZO
print "PIEZO TEST"
v = 200
sm.send_command(SerialMessageManager.SEND_COM_SETPIEZO, v)
while v < 400:
    print "PIEZO val", v
    v += 10
    sm.send_command(SerialMessageManager.SEND_COM_SETPIEZO, v)
    time.sleep(1)

print "TURN OFF"
sm.send_command(SerialMessageManager.SEND_COM_TURNOFF)

su.close()
