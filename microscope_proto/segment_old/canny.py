import cv2


def canny(im, t1, t2):
    #TODO gaussian blur first?
    return cv2.Canny(im, t1, t2, apertureSize=3)
