import cv2


def otsu(im):
    return cv2.threshold(im, 1, 255, cv2.THRESH_OTSU)[1]
