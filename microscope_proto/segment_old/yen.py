#YEN thresholding from imagej
import numpy as np
import cv2

def yen(im):
    # Implements Yen  thresholding method
    # 1) Yen J.C., Chang F.J., and Chang S. (1995) "A New Criterion 
    #    for Automatic Multilevel Thresholding" IEEE Trans. on Image 
    #    Processing, 4(3): 370-378
    # 2) Sezgin M. and Sankur B. (2004) "Survey over Image Thresholding 
    #    Techniques and Quantitative Performance Evaluation" Journal of 
    #    Electronic Imaging, 13(1): 146-165
    #    http:#citeseer.ist.psu.edu/sezgin04survey.html
    #
    # M. Emre Celebi
    # 06.15.2007
    # Ported to ImageJ plugin by G.Landini from E Celebi's fourier_0.8 routines
    threshold, ih, it = 0, 0, 0
    crit, max_crit = 0, 0
    norm_histo = np.array([0] * 256, np.float32) #/* normalized histogram */
    P1 = np.array([0] * 256, np.float32) #/* cumulative normalized histogram */
    P1_sq = np.array([0] * 256, np.float32) 
    P2_sq = np.array([0] * 256, np.float32) 
    total = 0
    imhist = cv2.calcHist([im], [0], mask=None, histSize=[256],ranges=[0, 256])
    data = imhist
    total = sum(data)[0]
    norm_histo = [x[0] / total for x in data]
    P1[0]=norm_histo[0];

    for ih in range(1, 256):
        P1[ih]= P1[ih-1] + norm_histo[ih]

    P1_sq[0]=norm_histo[0]*norm_histo[0]
    for ih in range(1, 256):
        P1_sq[ih]= P1_sq[ih-1] + norm_histo[ih] * norm_histo[ih];

    P2_sq[255] = 0.0
    
    for ih in range(254, -1, -1): 
        P2_sq[ih] = P2_sq[ih + 1] + norm_histo[ih + 1] * norm_histo[ih + 1];

    #/* Find the threshold that maximizes the criterion */
    threshold = -1
    max_crit = 0.0000000;
    for it in range(256): 
        crit = -1.0 * (np.log( P1_sq[it] * P2_sq[it]) if ( P1_sq[it] * P2_sq[it] )> 0.0 else 0.0) +  2 * (np.log(  P1[it] * ( 1.0 - P1[it] ) ) if ( P1[it] * ( 1.0 - P1[it] ) )>0.0 else 0.0)
       # print it, "crit is", crit
       #raw_input()
        if  crit > max_crit: 

            max_crit = crit;
            threshold = it;
    return threshold, cv2.threshold(im, threshold, 255, cv2.THRESH_BINARY)[1]

if __name__=='__main__':
    p = "/home/jeyson/Downloads/ti/"
    im = cv2.imread(p + "%s.bmp"%(10), 0)
    #im = cv2.subtract( np.ones_like(im) * 255, im)
    t, imt = yen(im)
    #imt = cv2.threshold(im, 115, 255, cv2.THRESH_BINARY)[1]
    print t
    cv2.imshow("test", imt)
    cv2.waitKey(0)
