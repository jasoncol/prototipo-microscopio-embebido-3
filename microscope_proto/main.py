'''Script principal donde se programa e inicia la interfaz web del sistema. Este es el script que se debe iniciar con python main.py  '''
from core_instance import cvis, config_file, start, stop #CORE VISION
import time
import web
render = web.template.render('templates/')
import cv2

urls = (
  '/', 'index',
  '/restart', 'restart',
  '/last_image', 'last_image',
  '/grab', 'grab',
  '/stream', 'stream',
  '/lastlog', 'lastlog',
  '/setled_auto', 'setled_auto',
  '/segment', 'segment',
  '/hough', 'hough',
  '/measure', 'measure',
  '/fourier', 'fourier',
  '/hologram', 'hologram',
  '/superres', 'superres',
  '/setpiezo', 'setpiezo',
  '/setled', 'setled',

)

def encode_img(im):
    ''' Helper to encode CV image in JPEG '''
    #im = cv2.resize(im, (640, 480)) 
    web.header("Content-Type", "image/jpg")
    cv2.imwrite('./last_image.png', im)
    im = cv2.imencode(".jpg", im)[1]
    return  im.tostring()


#Applications definitions
class index:
    ''' Main Webpage view (Home) '''

    def GET(self):
        with open(config_file, 'r') as f:
            config_data = f.read()
            stm = cvis.system
            camera = cvis.acq_driver +" (%s)"%(cvis.has_camera) 
            serial = "%s"%(cvis.has_serial)
            has_image =  './grab?v=%s&nograb=1'%(time.time()) if cvis.last_frame is not None else './static/noimage.png'
            return render.index(config_data, stm, camera, serial, has_image)
                
    def POST(self):
        i = web.input()
        cfg = i['config']
        with open(config_file, 'w') as f:
            f.write(cfg)
            raise web.seeother("/restart")


class restart:
    ''' Restart system '''
    def GET(self):
        global cvis
        stop(cvis)
        cvis = start()
        raise web.seeother("/") #Redirect to index

class last_image:
    ''' get last processed image '''
    def GET(self):
        return encode_img(cv2.imread('last_image.png'))

class grab:
    ''' grab camera frame interface  '''
    
    def GET(self):
        nograb = True if 'nograb' in web.input() else False
        if not nograb:
            im  = cvis.grab()
        else:
            im = cvis.last_frame

        #im = cv2.resize(im, (640, 480))
        web.header("Content-Type", "image/jpg")
        im = cv2.imencode(".jpg", im)[1]
        return  im.tostring()

class stream:
    def GET(self):
        return render.stream()

class segment:
    def GET(self):
        return encode_img(cvis.segment())

class hough:
    def GET(self):
        return encode_img(cvis.segment_hough())

class measure:
    def GET(self):
        return encode_img(cvis.measure_particles())

class setled_auto:
    def GET(self):
        return encode_img(cvis.setled_auto())

class setled:
    def GET(self):
        value = int(web.input()['value'])
        cvis.setled(value)
        return encode_img(cvis.grab())

class setpiezo:
    def GET(self):
        value = int(web.input()['value'])
        cvis.setpiezo(value)
        return encode_img(cvis.grab())

class fourier:
    def GET(self):
        return encode_img(cvis.fft_fourier())

class hologram:
    def GET(self):
        return encode_img(cvis.hologram_propagation())

class superres:
    def GET(self):
        return encode_img(cvis.hologram_superres())


class view:
    def GET(self):
        return ''

class lastlog:
    def GET(self):
        return cvis.last_log

if __name__== "__main__":
    print "Main Core vision started"
    app = web.application(urls, globals())
    app.run()
