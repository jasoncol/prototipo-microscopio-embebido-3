'''
Region props. Basic binary regionprops with basic shape, intensity properties

Use:
    - get_regions: To obtain a list of regions (class Region). pass a binary image (segmentation) to obtain the contours and the gray image that is used to calculate intensity properties. See Region class properties.
    - get_contours: To obtain a list of contours from the binary image. This is just a shortcut function for cv2.findcontours.
'''

import cv2
import numpy as np
import math
#import scipy.ndimage
#from scipy.stats import kurtosis, skew

#cv2.namedWindow('WORKAROUND_MAHOTAS_SEGFAULT', 0)#TODO mahotas is causing a segfault with cv2 highgui. This workaround solves it.
#import mahotas
import sys
from numpy.fft import fft
#from scipy.interpolate import interp1d
#import pdb
import time


PROPS_FUNCS = {'area', }


def get_regions(imbin, imgray=None, immask=None, imcolor=None, regclass=None):
    '''
    Get regions from the binary image using opencv find contours algorithm. The returned value is a list of Region
    objects that has several propertiesfrom the region. 
    Use regclass to use a custom Region class (the class must inherit from Region)
    '''
    if immask is not None:
        imbin = imbin * (immask/255)
    imbin_inv = cv2.subtract(np.ones(imbin.shape, np.uint8) * 255, imbin)
    
    if immask is not None:
        imbin_inv = imbin_inv * (immask/255)
    
    contours, h = get_contours(imbin, True, True)
    class_ = regclass or Region
    regions = [class_(x, imbin, imgray, imbin_inv, immask, h[i], imcolor) for i, x in enumerate(contours)]
    return regions


def get_contours(imbin, ignore_holes=True, ret_hier=False):
    ''' Return contours 
        Get list of contours.

    ignore_holes=True to ignore contours inside contours (holes)
    '''
    cnt, h = cv2.findContours(imbin.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
    if len(cnt) == 0:
        ret = ([], []) if ret_hier else []
        return ret
    cnt2  = zip(cnt, h[0])
    if ignore_holes:
        contours = [x[0] for x in cnt2 if x[1][3] <= -1]
    else:
        contours = [x[0] for x in cnt2]
    if ret_hier:
        return contours, h[0]
    else:
        return contours


def lazyprop(fn):
    '''Lazy property. To create lazy properties'''
    attr_name = fn.__name__ + '_'
    @property
    def _lazyprop(self):
        val = fn(self)
        if not self.store_prop:
            return val # do not store but return
        if not hasattr(self, attr_name):
            setattr(self, attr_name, val)
        val = getattr(self, attr_name)
        if val is None:
            setattr(self, attr_name, fn(self))
            val = getattr(self, attr_name)
        return val
    return _lazyprop


cti = 0
class Region(object):
    ''' A region is a set of connected pixels from a  2D image obtained from a contour.
    The instances of this class require a contour, a binary image (where the contour was obtained from) and the original grayscale image.
    The class uses python properties mainly (if a feature requires parameters then a method is used instead). The properties are lazy (they're only calculated once when the property is requested/readed for the first time). Properties are read only. 
    
    Example:
    r = Region(...) #pass required params
    print r.area #calculates and returns area. Area value is now stored internally.
    '''
    store_prop = True
    MIN_BBOX_LENGTH = 10 #min size of a big bbox dimension
    def __init__(self, contour, imbin, imgray, imbin_inv, mask=None, cnt_hierarchy=None, imcolor=None):
        '''
        '''
        self.cnt_hier = cnt_hierarchy
        self.imgray = imgray 
        self.imcolor= imcolor
        self.contour = contour
        self.cnt = contour
        self.mask = mask
        self.mask_norm =  self.mask / 255 if mask is not None else None
        self.imbin = imbin
        self.imbin_inv = imbin_inv
    #TODO assertions
    @lazyprop
    def centroid(self):
        if self.moments['m00'] != 0.0:
            cx = self.moments['m10'] / self.moments['m00']
            cy = self.moments['m01'] / self.moments['m00']
            return (cx, cy)
        else:
            #"Region has zero area (moment)"
            return self.cnt[0][0][0], self.cnt[0][0][1] #use first point of countour

    
    @lazyprop
    def moments(self):
        ''' contour moments '''
        return cv2.moments(self.cnt)

    @lazyprop
    def humoments(self):
        ''' contour hu moments. Log transform is used on the moments: -np.sign(hum) * np.log10(np.abs(hum))'''
        hum =cv2.HuMoments(self.moments)
        hm = -np.sign(hum) * np.log10(np.abs(hum))
        ret = []
        for i in hm:
            ret.append(i[0])
        return ret

    
    @lazyprop
    def moments_int(self):
        ''' region intensity moments '''
        return cv2.moments(self.region_fillim)

    @lazyprop
    def humoments_int(self):
        ''' region intensity hu moments. The moments are transformed with log transform: -np.sign(hum) * np.log10(np.abs(hum)) '''
        hum = cv2.HuMoments(self.moments_int)
        hm = -np.sign(hum) * np.log10(np.abs(hum))
        ret = []
        for i in hm:
            ret.append(i[0])
        return ret
    @lazyprop
    def perimeter(self):
        return cv2.arcLength(self.cnt, True)
    
    @lazyprop
    def moments_area(self): 
        '''this is area calculated from moments'''
        return cv2.contourArea(self.cnt)

    @lazyprop
    def area(self): 
        ''' number of pixels '''
        #return self.filled_image.sum()/255
        return self.filled_image_bbox.sum()/255
        #return len(self.filled_image[self.filled_image > 0])
        #return self.filled_image.nonzero()[0].shape[0]
    @lazyprop
    def bounding_box(self):
        return cv2.boundingRect(self.cnt)
    
    @lazyprop
    def bbox_x(self):
        return self.bounding_box[0]
    
    @lazyprop
    def bbox_y(self):
        return self.bounding_box[1]
    
    @lazyprop
    def bbox_w(self):
        return self.bounding_box[2]

    @lazyprop
    def bbox_h(self):
        return self.bounding_box[3]
    
    @lazyprop
    def aspect_ratio(self):
        return self.bbox_w / float(self.bbox_h)
    
    @lazyprop
    def equiv_diameter(self):
        return np.sqrt(4 * self.area / np.pi)
    
    @lazyprop
    def extent(self):
        return self.area / (self.bbox_w * float(self.bbox_h))
    
    @lazyprop
    def convex_hull(self):
        return cv2.convexHull(self.cnt)

    @lazyprop
    def convex_area(self):
        ''' convex hull area based on moments '''
        return cv2.contourArea(self.convex_hull)
    
    @lazyprop
    def solidity(self):
        return self.area / float(self.convex_area) #TODO area or moment_area?
    
    @lazyprop
    def ellipse(self):
        return cv2.fitEllipse(self.cnt)
    
    @lazyprop
    def ellipse_center(self):
        return self.ellipse[0]
    
    @lazyprop
    def ellipse_axes(self):
        return self.ellipse[1]
    
    @lazyprop
    def ellipse_orientation(self):
        return self.ellipse[2]
    @lazyprop
    def orientation(self):
        m = self.moments
        o = 0.5 * math.atan((2.0* m['m11']) / (m['m20'] - m['m02'])) * (180/3.141592)
        return o

    @lazyprop
    def ellipse_major_axis(self):
        return max(self.ellipse_axes)
    
    @lazyprop
    def ellipse_minor_axis(self):
        return min(self.ellipse_axes)
    
    @lazyprop
    def eccentricity(self):
        try:
            return np.sqrt(1-(self.ellipse_minor_axis / (self.ellipse_major_axis+1))**2)
        except:
            print "ERROR ON excentricity"
            return 0
    @lazyprop
    def circularity(self):
        try:
            return (4 * np.pi * self.moments_area)/(self.perimeter ** 2)
        except:
            return 0
    @lazyprop
    def approx_poly(self):
        return  cv2.approxPolyDP(self.cnt,0.02 * self.perimeter,True)

    @lazyprop
    def filled_image(self):
        ''' return imbin (binary image) with this region only (the rest in blak) '''
        filled_im = np.zeros(self.imbin.shape, np.uint8)
        cv2.drawContours(filled_im,[self.contour],0,255,-1)
        return filled_im
    @lazyprop
    def fillim_invbin(self):
        ''' filled_image + inverted binary. This results in the inverted segmentation except this region '''
        return self.filled_image + self.imbin_inv

    @lazyprop
    def contour_image(self):
        contour_im = np.zeros(self.imbin.shape, np.uint8)
        cv2.drawContours(contour_im,[self.contour],0,255,1)
        return contour_im

    @lazyprop
    def filled_image_bbox(self):
        ''' return a binary image with the size of the boundig box with the region's pixels white. NOT imp. TODO'''
        bx, by, bw, bh = self.bounding_box
        return self.filled_image[by:by+bh, bx:bx+bw]
    @lazyprop
    def filled_image_bigbox(self):
        ''' return a binary image with the size of the big boundig box with the region's pixels white. NOT imp. TODO'''
        bx, by, bw, bh = self.big_bbox
        return self.filled_image[by:by+bh, bx:bx+bw]
   
    @lazyprop
    def region_fillim(self):
         ''' Gray image masked with filled image. The result is an image with the same size of imgray but with the region's pixels only (the rest are zero) '''

         return self.imgray * self.filled_image.astype(np.bool)
    
    
    @lazyprop
    def region_bbox_fillim(self):
        ''' Gray image masked and cropped with bounding box '''
        bx, by, bw, bh = self.bounding_box
        return self.imgray[by:by+bh, bx:bx+bw] * self.filled_image_bbox.astype(np.bool)

    @lazyprop
    def region_bfill(self):
        ''' Gray image not masked and cropped with bounding box '''
        bx, by, bw, bh = self.bounding_box
        return self.imgray[by:by+bh, bx:bx+bw] #* self.filled_image_bbox.astype(np.bool)

    @lazyprop
    def region_color_bbox_fillim(self):
        ''' Color image masked and cropped with bounding box '''
        bx, by, bw, bh = self.bounding_box
        return self.imcolor[by:by+bh, bx:bx+bw]# * self.filled_image_bbox.astype(np.bool)

    @lazyprop
    def region_bigbox_fillim(self):
        ''' Gray image masked and cropped with big bounding box '''
        bx, by, bw, bh = self.big_bbox
        return self.imgray[by:by+bh, bx:bx+bw] * self.filled_image_bigbox.astype(np.bool)

    @lazyprop
    def convex_image(self):
        cim = np.zeros(self.imbin.shape[0:2],np.uint8)
        cv2.drawContours(cim,[self.convex_hull],0,255,-1)
        return cim
    
    @lazyprop
    def pixel_list(self):
        a = np.transpose(np.nonzero(self.filled_image_bbox))
        bx, by, bw, bh = self.bounding_box
        py = a[..., 0]
        px = a[..., 1]

        py += by
        px += bx
        return py, px
        
        #b = np.transpose(np.nonzero(self.filled_image))
    
    @lazyprop
    def minmax_loc(self):
        return cv2.minMaxLoc(self.imgray, mask=self.filled_image)
    @lazyprop
    def min_val(self):
        return self.minmax_loc[0]
    @lazyprop
    def max_val(self):
        return self.minmax_loc[1]
    @lazyprop
    def mean_val(self):
        return cv2.mean(self.imgray, self.filled_image)[0]
    
    @lazyprop
    def std_dev(self):
        return self.imgray[self.filled_image > 0].std()
    
    @lazyprop
    def extreme_points(self):
        ''' extreme points location. leftmost, rightmost, topmost, bottommost'''
        return tuple(self.cnt[self.cnt[:,:,0].argmin()][0]), \
               tuple(self.cnt[self.cnt[:,:,0].argmax()][0]), \
               tuple(self.cnt[self.cnt[:,:,1].argmin()][0]), \
               tuple(self.cnt[self.cnt[:,:,1].argmax()][0])

    @lazyprop
    def filled_image_inv(self):
        ''' Inverted filled image '''
        ones = np.ones(self.filled_image.shape, np.uint8) * 255
        fim_inv = cv2.subtract(ones, self.filled_image)
        if self.mask is not None:
            fim_inv[self.mask == 0] = 0
        return fim_inv

    @lazyprop
    def color_hsv(self):
        ''' HSV color features '''
        im_hsv = cv2.cvtColor(self.imcolor, cv2.COLOR_BGR2HSV)
        h = im_hsv[self.filled_image > 0, 0].mean()
        s = im_hsv[self.filled_image > 0, 1].mean()
        v = im_hsv[self.filled_image > 0, 2].mean()

        return h, s, v 

    @lazyprop
    def color_bands(self):
        b = self.imcolor[self.filled_image > 0, 0].mean()
        g = self.imcolor[self.filled_image > 0, 1].mean()
        r = self.imcolor[self.filled_image > 0, 2].mean()
        return b, g, r


    @lazyprop
    def contrast_features(self):
        ''' Weber contrast (Fg - Bg) / Bg and others. Uses double sized bbox to get the background neighborh '''
        bbb = self.big_bbox
        bbind = bbb[1], bbb[1] + bbb[3], bbb[0], bbb[0] + bbb[2] #bounding box in terms of indexes of the image matrix

        #using dilation
        r = 13
        dil = cv2.dilate(self.filled_image, np.ones((r, r)))
        dil_bg = (dil - self.imbin)  * self.mask_norm
            
        bground_mean = cv2.mean(self.imgray[bbind[0]:bbind[1], bbind[2]:bbind[3]], 
                self.imbin_inv[bbind[0]:bbind[1], bbind[2]:bbind[3]])[0]

        bground_mean = cv2.mean(self.imgray, dil_bg)[0]
        wc = (self.mean_val - bground_mean) / (bground_mean + 1)
        lum_ratio = self.mean_val / (bground_mean +1)
        mod = (self.mean_val - bground_mean) / (self.mean_val + bground_mean + 1)
        dif = self.mean_val - bground_mean
        return [wc, lum_ratio, mod,  dif]
    
    
    @lazyprop
    def big_bbox(self):
        ''' double sized bounding box of the contour '''
        bbox = self.bounding_box
        big_bbox = [0, 0, 0, 0] 
        big_bbox[0] = max(1, bbox[0] - math.ceil(bbox[2]/2))
        big_bbox[1] = max(1, bbox[1] - math.ceil(bbox[3]/2))
        big_bbox[2] = 2 * bbox[2]
        big_bbox[3] = 2 * bbox[3]
        if big_bbox[2] < self.MIN_BBOX_LENGTH:
           big_bbox[0] = big_bbox[0] - math.ceil((self.MIN_BBOX_LENGTH-big_bbox[2]) / 2) 
           big_bbox[2] = big_bbox[2]  + 2 * math.ceil((self.MIN_BBOX_LENGTH-big_bbox[2]) / 2)

        if big_bbox[3] < self.MIN_BBOX_LENGTH:
           big_bbox[1] = big_bbox[1] - math.ceil((self.MIN_BBOX_LENGTH-big_bbox[3]) / 2) 
           big_bbox[3] = big_bbox[3]  + 2 * math.ceil((self.MIN_BBOX_LENGTH-big_bbox[3]) / 2)

        if big_bbox[2] + big_bbox[0] > self.imbin.shape[1]:
           big_bbox[2] = self.imbin.shape[1] - big_bbox[0] -1

        if big_bbox[3] + big_bbox[1] > self.imbin.shape[0]:
           big_bbox[3] = self.imbin.shape[0] - big_bbox[1] -1
        #TODO padding to achieve centered bbox
        big_bbox = [ int(x) for x in big_bbox]
        return big_bbox

    @lazyprop
    def mean_boundary_gradient(self):
        ''' Mean value of the gradient at the boundary region's pixels '''
        #cnt = self.cnt
        bbb = self.big_bbox
        bbind = bbb[1], bbb[1] + bbb[3], bbb[0], bbb[0] + bbb[2] #bounding box in terms of indexes of the image matrix
        imgray_bbox = self.imgray[bbind[0]:bbind[1], bbind[2]:bbind[3]]
        filled_bbox = self.contour_image[bbind[0]:bbind[1], bbind[2]:bbind[3]]
        sobel = cv2.Sobel(imgray_bbox, cv2.CV_32F,1, 1, ksize=3)
        try:
            mbg = cv2.mean(abs(sobel), filled_bbox)[0]
        except:
            mbg = 0
        return mbg
    
    @lazyprop
    def bestprofileline(self):
        '''best profile line using horizontal, vertical and diagonals lines delimited by the big bounding box '''
        bx, by, bw, bh = self.big_bbox
        x0, y0 = bx, by + bh/2
        x1, y1 = bx + bw,  y0
        hor_line = ((x0,y0), (x1, y1))
        
        x0, y0 = bx + bw/2, by 
        x1, y1 = x0, by + bh
        ver_line = ((x0,y0), (x1, y1))
        
        x0, y0 = bx, by
        x1, y1 = bx + bw, by + bh
        diag1_line = ((x0,y0), (x1, y1))

        x0, y0 = bx + bw, by
        x1, y1 = bx, by + bh
        diag2_line = ((x0,y0), (x1, y1))
        #cv2.imshow("current", self.overlay_region(filled=False))
        pfh = profile_line(hor_line[0], hor_line[1], self.imgray)
        pfv = profile_line(ver_line[0], ver_line[1], self.imgray)
        pfd = profile_line(diag1_line[0], diag1_line[1], self.imgray)
        pfb = profile_line(diag2_line[0], diag2_line[1], self.imgray)

        md = 9999
        bestline = 0
        for line in pfh, pfv, pfd, pfb:
            l1, l2 = (int(line[0]) + int(line[1]) + int(line[2]))/ 3.0, (int(line[-1]) + int(line[-2]) + int(line[-3])) / 3.0
            d = abs(l1 - l2)
            if d < md:
                md = d 
                bestline = line

        #print "best d", md
        return bestline
    
    @lazyprop
    def bestpl_fourier(self):
        numpoints = 30 #TODO parameter
        bpl = self.bestprofileline - self.bestprofileline.min()
        xvals = range(len(bpl))

        cp, px, py, cd = [], [], [], []
        xv, yv = interp_points(xvals, bpl, numpoints)

        for point in zip(xv, yv):
            pnt = point[0] , point[1] 
            cp.append(complex(pnt[0], pnt[1]))

        
        if len(cp) == 1:
           cp *= numpoints
        #FFT Coordinates
        fd_coords = fft(cp)
        fm = abs(fd_coords[1]) # magnitude of FD1
        #normalization
        fd_coords = [abs(x)/fm for x in fd_coords[2: 12]] #FD2/|FD1|, FD3/|FD2|, ...

        return fd_coords


    @lazyprop
    def bestpl_maxminavg(self):
        bpl = self.bestprofileline
        return bpl.max(), bpl.min(), bpl.mean(), bpl.std()    
        
    @lazyprop
    def bestprofileline_ellipse (self):
        ''' #TODO profile line using fitted ellipse's axis '''
        #TODO
        return 0
    @lazyprop
    def skewness(self):
        ''' skewness of the region + neighborhood (double bbox) '''
        bbb = self.big_bbox
        bbind = bbb[1], bbb[1] + bbb[3], bbb[0], bbb[0] + bbb[2] #bounding box in terms of indexes of the image matrix
        img = self.imgray[bbind[0]:bbind[1],bbind[2]:bbind[3]]
        img_mask =  self.fillim_invbin[bbind[0]:bbind[1],bbind[2]:bbind[3]]/255
        data = img[img_mask > 0] #mask pixels that are outside the mask (if any) and belong to another segmentation

        sk = skew(data)
        return sk

    @lazyprop
    def kurtosis(self):
        ''' skewness of the region + neighborhood (double bbox) '''
        bbb = self.big_bbox
        bbind = bbb[1], bbb[1] + bbb[3], bbb[0], bbb[0] + bbb[2] #bounding box in terms of indexes of the image matrix
        img = self.imgray[bbind[0]:bbind[1],bbind[2]:bbind[3]]
        img_mask =  self.fillim_invbin[bbind[0]:bbind[1],bbind[2]:bbind[3]]/255
        data = img[img_mask > 0]


        return 0.0 #kurtosis(data)
    
    @lazyprop
    def bestpl_skewness(self):
        '''best profile line skewness'''
        return 0.0 #skew(self.bestprofileline)

    @lazyprop
    def bestpl_kurtosis(self):
        '''best profile line kurtosis'''
        return 0.0 #kurtosis(self.bestprofileline)
    

    @lazyprop
    def haralick_features_old(self):
        '''Haralick features. uses dilation (radius 5) on the region to incorporate a few pixels of the surrounding neighborhood ''' 
        reg_dil = cv2.dilate(self.filled_image,np.ones((5,5)), iterations=1)
        im_rg = self.imgray * (reg_dil/255 ) #mask non region pixels
        #har = mahotas.features.haralick(im_rg, ignore_zeros=True) #compute haralick 
        #cv2.imshow("testtt", im_rg)

        #print "HAR", har
        #cv2.waitKey(0)
        return []#list(har.mean(0))


    @lazyprop
    def haralick_features(self):
        '''Haralick features. uses dilation (radius 5) on the region to incorporate a few pixels of the surrounding neighborhood ''' 
       #cv2.imshow("testtt", im_rg)

        #print "HAR", har
        #cv2.waitKey(0)
        return list(self.haralick.mean(0))
    @lazyprop
    def haralick(self):
        reg_dil = cv2.dilate(self.filled_image,np.ones((5,5)), iterations=1)
        im_rg = self.imgray * (reg_dil/255 ) #mask non region pixels
        im_rg = self.imgray
        har = mahotas.features.haralick(im_rg, ignore_zeros=True) #compute haralick 
        return  har
    @lazyprop
    def coocurrence_matrix(self):
        from mahotas.features.texture import cooccurence
        direction = [(0,1), (1,1), (1,0), (1,-1)]
        res = []
        for d in range(len(direction)):
            c = cooccurence(self.imgray, d, symmetric=False)
            res.append(c)
        return res
        
    @lazyprop
    def lbp(self):
        pass

    @lazyprop
    def gabor_filters(self):
        """ Gabor filters based on Mery's Balu """
        L  = 8 # Number of rotations
        S  = 8 # Number of dilations
        fh = 2.0 # High freq of interest
        fl = 1.0 # Lower freq od interest
        M  = 21 # Mask size
        I = self.region_bbox_fillim.astype(np.float32)
        R = self.filled_image_bbox

        alpha = (fh/fl)**(1.0/(S-1.0));
        sx = np.sqrt(2*np.log(2))*(alpha+1)/2/np.pi/fh/(alpha-1)
        sy = np.sqrt(2*np.log(2)-(2*np.log(2)/2/np.pi/sx/fh)**2)/(2*np.pi*np.tan(np.pi/2.0/L)*(fh-2*np.log(1/4.0/np.pi**2/sx ** 2/fh)))
        u0 = fh
   
        k = R>0
       
        g = np.zeros((S, L), np.float32)
        size_out_y = (M - 1)/2 #I.shape[0] + M - 1
        size_out_x = (M - 1)/2 #I.shape[1] + M - 1
        Ipad = np.pad(I, ((size_out_y, size_out_y),(size_out_x, size_out_x) ), 'constant')
        sfy, sfx =  1+(Ipad.shape[0] - M-1)/ 2, 1 +(Ipad.shape[1] - M-1 ) / 2
        Iw = np.fft.fft2(Ipad) #,size_out(1),size_out(2)) #TODO crop image?
        n1 = (M+1)/2.0;
        NN,MM = I.shape
        #from matplotlib import pyplot as plt

        for p in xrange(S):
            for q in xrange(L):
                f = self.Bgabor_pq(p,q,L,sx,sy,u0,alpha,M)
                freal_pad = np.pad(np.real(f), ((sfy, sfy), (sfx, sfx)), 'constant')
                fimag_pad = np.pad(np.imag(f), ((sfy, sfy), (sfx, sfx)), 'constant')
                #plt.imshow(abs(f))
                #plt.show()
                freal_pad = freal_pad[0:Ipad.shape[0], 0:Ipad.shape[1]]
                fimag_pad = fimag_pad[0:Ipad.shape[0], 0:Ipad.shape[1]]

                Ir = np.real(np.fft.ifft2(Iw*np.fft.fft2(freal_pad)))  #,size_out[1),size_out(2))))
                Ii = np.real(np.fft.ifft2(Iw*np.fft.fft2(fimag_pad)))  #,size_out(1),size_out(2))))
                Ir = Ir[n1:n1+NN,n1:n1+MM]
                Ii = Ii[n1:n1+NN,n1:n1+MM]
                Iout = np.sqrt(Ir*Ir + Ii*Ii)
                g[p,q] = Iout[k].mean()

        gmax = g.max()
        gmin = g.min()
        J = (gmax-gmin)/float(gmin)
        X = g[:], gmax,  gmin,  J

        return X

    def  Bgabor_pq(self, p,q,L,sx,sy,u0,alpha,M):
        """ Gabor Kernel """
        f = np.zeros((M,M), np.complex)

        sx2 = sx*sx
        sy2 = sy*sy
        c = (M+1)/2.0
        ap = alpha**-p
        tq = np.pi*(q-1)/L
        f_exp = 2 * np.pi * 1j * u0 #np.sqrt(-1)*u0

        xm, ym = np.meshgrid(range(M), range(M))
        '''
        for i in xrange(M):
            x = i - c
            for j in xrange(M):
                y = j - c;
                x1 = ap*(x*np.cos(tq)+y*np.sin(tq));
                y1 = ap*(y*np.cos(tq)-x*np.sin(tq));
                f[i,j] = np.exp(-0.5*(x1*x1/sx2+y1*y1/sy2))*np.exp(f_exp*x1)
        '''
        x = xm - c
        y = ym - c
        x1 = ap*(x*np.cos(tq)+y*np.sin(tq));
        y1 = ap*(y*np.cos(tq)-x*np.sin(tq));

        f = np.exp(-0.5*(x1*x1/sx2+y1*y1/sy2))*np.exp(f_exp*x1)
        f = ap*f/2/np.pi/sx/sy;
        return f

    def fourier_contour(self, numpoints=30):
        '''Method. fourier descriptors of contour points.
        Two kinds of fft are calculated:
        1. Coordinates: Each point (x, y) is centered by substracting the centroid and transformed to a complex number  x + iy. TODO: number of points normalization not implemented for this one.
        http://knight.temple.edu/~lakamper/courses/cis601_2008/etc/fourierShape.pdf
        2. Centroid distance: The distance of the contour point to the centroid is used to form the series from where the fft is calculated.
        return list(fft(coords), fft(centroid_dist)).
        '''
        cx, cy = self.centroid
        cp, px, py, cd = [], [], [], []
        for point in self.cnt:
            pnt = point[0][0] - cx, point[0][1] -cy
            cp.append(complex(pnt[0], pnt[1]))
            cd.append(math.sqrt(pnt[0]**2 + pnt[1]**2))

        c = [float(x) for x in xrange(len(self.cnt))]
        #print "c", c
        #print "cd", cd
        c, cd = interp_points(c, cd, numpoints)
        cd_complex = [complex(x, cd[i]) for i, x in enumerate(c)]
        
        if len(cp) == 1:
           cp *= numpoints
        #FFT Coordinates
        fd_coords = fft(cp)
        fm = abs(fd_coords[1]) # magnitude of FD1
        #normalization
        fd_coords = [abs(x)/fm for x in fd_coords[2:]] #FD2/|FD1|, FD3/|FD2|, ...
        #FFT centroid distances
        fd_cd = fft(cd_complex)
        fm = abs(fd_cd[1]) # magnitude of FD1
        #normalization
        fd_cd = [abs(x)/fm for x in fd_cd[2:]] #FD2/|FD1|, FD3/|FD2|, ...

        return [fd_coords, fd_cd]

    #TODO distance_image
    def overlay_region(self, color=(0, 0, 255), imcolor=None, filled=True):
        ''' returns gray image with regions overlayed on it '''
        if imcolor is None:
            imcolor = cv2.cvtColor(self.imgray, cv2.COLOR_GRAY2RGB)
        elif len(imcolor.shape) == 2:
            imcolor = cv2.cvtColor(imcolor, cv2.COLOR_GRAY2RGB)
            
        imc = self.filled_image if filled else self.contour_image
        imr = np.zeros(imcolor.shape, np.uint8)
        imr[imc > 0] =  list(color)

        return cv2.addWeighted(imcolor, 1, imr, 0.5, 0)

    def __repr__(self):
        return "Region located at x=%s, y=%s. Area=%s"%(self.centroid[0], self.centroid[1], self.area)


def profile_line(p1, p2, imgray):
    ''' profile line pixel intesities. p1 = (x0, y0), p2 = (x1, y1) '''
    num = 35
    x0, y0 = p1
    x1, y1 = p2
    x, y = np.linspace(x0, x1, num), np.linspace(y0, y1, num)
    #zi = scipy.ndimage.map_coordinates(imgray, np.vstack((x,y)))
    
    zi = imgray[y.astype(np.int), x.astype(np.int)]
    '''
    fig, axes = plt.subplots(nrows=2)
    axes[0].imshow(imgray)
    axes[0].plot([x0, x1], [y0, y1], 'ro-')
    axes[0].axis('image')
    axes[1].plot(zi)
    print zi
    print "DIFERENCE", int(zi[0]) - int(zi[-1])
    plt.show()
    '''
    return zi

def interp_points(x, y, num):
    if len(x)< 2:
        x = x * num
        y = y * num
        return x, y

    #f2 = interp1d(x, y, kind='linear')
    xnew = np.linspace(0, max(x), num)
    return xnew, 0#f2(xnew)


def overlay_regions(regions, imgray, color=(0,0,255), legends=None, filled=True): #TODO slow
    if len(imgray.shape) < 3:
        imc = cv2.cvtColor(imgray, cv2.COLOR_GRAY2RGB)
    else:
        imc = imgray
    imn = np.zeros(imc.shape, np.uint8)
    contours = [x.cnt for x in regions]
    t = -1 if filled else 1
    cv2.drawContours(imn, contours, -1, color, t)
    overlay_reg = cv2.addWeighted(imc,1.0,  imn, 0.5, 0)
    if legends is not None:
        for i, t in enumerate(legends):
            cv2.putText(overlay_reg, t, (int(regions[i].centroid[0]), int(regions[i].centroid[1])), cv2.FONT_HERSHEY_COMPLEX_SMALL, 0.5, (0,255,0),1)
    return overlay_reg
    
