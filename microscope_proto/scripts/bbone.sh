#!/bin/bash
#Activate UART slots (BBONE kernel 3.8)
echo BB-UART1 >/sys/devices/bone_capemgr*/slots
echo BB-UART2 >/sys/devices/bone_capemgr*/slots
echo BB-UART4 >/sys/devices/bone_capemgr*/slots
echo BB-UART5 >/sys/devices/bone_capemgr*/slots
