from acquisition.thorlabs import ThorlabsAcquisition
import cv2
import time

thor_adq = ThorlabsAcquisition()
cnt = 0
while True:
    print "grabbing image"
    frm = thor_adq.grab()
    if frm is not None:
        print "image grabbed"
        #print "max", frm.max(), "min", frm.min()
        frm = cv2.resize(frm, (640, 480))
        cv2.imshow("test", frm)
        k = cv2.waitKey(200)#raw_input("select")
        k = 'r'
        print "k is ", k
        if k == 'z':
            print "saving"
            cnt += 1
            #save image
            cv2.imwrite("out_piezo/holograma/%s.bmp"%(cnt), frm)
