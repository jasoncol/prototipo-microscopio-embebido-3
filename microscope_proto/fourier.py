import fftw3
import numpy as np
from fourier_cv import cv_dft, pad_fourier

def fft_cv(im):
    cv_dft(im)

def fft_w3(im):
    pad = pad_fourier(im)
    inputa = pad.astype(complex)
    outputa = np.zeros_like(inputa)

    fft = fftw3.Plan(inputa,outputa, direction='forward', flags=['measure'])
    fft.execute()

def fft_numpy(im):
    pad = pad_fourier(im)
    return np.fft.fft2(pad)

