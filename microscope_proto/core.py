import ConfigParser
import os
from acquisition.opencv_acq import OpencvAcquisition
from acquisition.thorlabs import ThorlabsAcquisition
from acquisition.disk_acq import DiskAcquisition
from acquisition.v4l2_acq import V4L2Acquisition
from pserial.serial_messages import SerialMessageManager
from pserial.uart import SerialUART
import segment as seg
import measures as meas
import hologram
import fourier
import superres
import cv2
import time
import numpy as np


BBONE = 'bbone'
BBONEXS = 'bbonexm'
FALCON = 'falcon'
PC = 'pc'
RASPBERRY = 'raspberry'

#Camera
THORLABS = 'thorlabs'
OPENCV = 'opencv' #preferido para webcam
DISK = 'disk'
V4L2 = 'webcam' #por si opencv no funciona

''' Decoradores para algunos metodos '''
def ensure_img(f):
    def inner(*args, **kwargs):
        core = args[0]
        core.last_frame = core.last_frame if core.last_frame is not None else core.grab()

        return f(*args, **kwargs)
    return inner

def ensure_segment(f):
    def inner(*args, **kwargs):
        core = args[0]
        core.last_segment = core.last_segment if core.last_segment is not None else core.segment_otsu()

        return f(*args, **kwargs)
    return inner


def log_errors(f):
    def inner(*args, **kwargs):
        core = args[0]
        try:
            r = f(*args, **kwargs)
            return r
        except Exception, e:
            print "ERROR: ", e, "Function", str(f)
            core.last_log = "Error: %s, Function: %s"%(e, str(f.__name__))
        return core.last_frame

    return inner


class CoreVision(object):
    ''' Sistema principal. Core de vision que realiza la inicializacion del sistema y verificacion de dispositivos '''


    def __init__(self, config_file, *args, **kwargs):
        print "Iniciando Sistema"
        self.modules = []
        print "Configuracion: ", config_file
        self.cfg = ConfigParser.RawConfigParser()
        self.cfg.read(config_file)
        self.last_frame = None
        self.last_segment = None
        self.last_holog = None
        self.last_log = ''
        self.has_camera =  False
        self.has_serial = False

        self.initialization()

    def register_procmodule(self, mod):
        self.modules.append(mod)

    def initialization(self):
	''' Inicializacion sistema '''
        print "Iniciando nucleo de vision"
        self.has_camera =  False
        self.has_serial = False

        try:
            system = self.cfg.get('general', 'system')
            self.system = system
            print "Sistema", system
            
            if system == BBONE:
                #run script to activate and configure GPIO in kernel
                os.system('./scripts/%s.sh'%(system))
                pass
            elif system == PC:
                pass
            self.system_info = os.system('./scripts/pc_info.sh')
        
            #Acquisition
            acq_driver = self.cfg.get('acquisition', 'driver')
            ca = self.configure_acquisition(acq_driver) #Configurar adquisicion
            
            self.has_camera = ca
            se = self.configure_serial(system) #Configurar comunicacion serial    
            self.has_serial = se
        except Exception, e:
            print "Error inicializando dispositivos"
            print e


    def close_cam(self):
        self.acq.close()

    def configure_serial(self, system):
        self.sm = None

        if system == BBONE:
            su = SerialUART()
            su.open(self.cfg.get('serial', 'device'))
            sm = SerialMessageManager(pserial=su, sendbin=False)
            self.sm = sm
            return True
        else:
            print "Serial not implemented in this platform!"
            return False

    def configure_acquisition(self, acq_driver):
        fps = self.cfg.getint('acquisition', 'fps')
        print "Configure acquisition: ", acq_driver

        acq = None
        try:
            if acq_driver == OPENCV:
               acq = OpencvAcquisition()
            elif acq_driver == THORLABS:
               acq = ThorlabsAcquisition()
               pc = self.cfg.getint('thorlabs', 'pixel_clock')
               et = self.cfg.getint('thorlabs', 'exposure_time')
               cm = self.cfg.getint('thorlabs', 'color_mode')
               acq.cam.SetFrameRate(fps)
               acq.cam.SetPixelClock(pc)
               acq.cam.SetExposureTime(et)
               acq.cam.SetColorMode(cm)
            elif acq_driver == V4L2:
               acq = V4L2Acquisition()

            elif acq_driver == DISK:
               acq = DiskAcquisition(path=self.cfg.get('acquisition', 'disk_folder'))
        except Exception, e:
           print "Error acquisition", e
           self.acq = None
           return False
            
        self.acq = acq
        self.acq_driver = acq_driver
        if acq is  None:
            return False
        return True
    
    def grab(self):
        print "Grabbing image"
        frame = self.acq.grab()
        max_acq = 0
        while frame is None and max_acq < 5: #Retry grab until valid frame
            frame = self.acq.grab()
            max_acq += 1 
        if frame is None:
            self.last_log = "Imagen no pudo ser adquirida"
            print self.last_log
            return None

        if len(frame.shape) == 3:
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        self.last_frame_ori = frame
        
        w, h, save = self.cfg.getint('acquisition', 'width'), self.cfg.getint('acquisition', 'height'), self.cfg.getboolean('acquisition', 'save')

        if save:
            dfolder = self.cfg.get('acquisition', 'disk_folder')
            #only save if driver is not disk
            if not self.acq_driver == DISK:
                fdest= dfolder + '/%s.png'%(time.time())
                print "Saving to", fdest
                cv2.imwrite(fdest, frame)

        if w > 0 and h > 0:
            frame = cv2.resize(frame, (w, h))
        self.last_frame = frame
        self.last_holog = frame #acquired img could be a holog, so we do this here as well

        self.last_log = 'Imagen adquirida en: %s'%(time.time())
        return frame
    
    ''' SEGMENTACION '''
    @ensure_img
    def segment(self):
        s = self.cfg.get('segmentation', 'method')
        method = {'thresh': self.segment_threshold,'canny': self.segment_canny, 'otsu': self.segment_otsu, 'yen': self.segment_yen, 'bhat': self.segment_bhat}
        k = time.time()
        im = method[s]() # call method
        if self.cfg.getboolean('segmentation', 'inv'):
            im = 255 - im
            self.last_segment = im
        self.last_log = "Segmentacion %s (%0.4f segundos). Invertida: %s"%(s, time.time()-k, self.cfg.getboolean('segmentation', 'inv'))
        return im

    @ensure_img
    def segment_threshold(self):
        t =  int(self.cfg.get('segmentation', 't'))
        self.last_segment = seg.threshold(self.last_frame, t) 
        return self.last_segment

    @ensure_img
    def segment_otsu(self):
        self.last_segment = seg.otsu(self.last_frame) 
        return self.last_segment

    @ensure_img
    def segment_bhat(self):
        w, T = int(self.cfg.get('segmentation', 'bhat_w')), int(self.cfg.get('segmentation', 'bhat_t'))
        self.last_segment = seg.bottom_hat(self.last_frame, w, T) 
        return self.last_segment

    @ensure_img
    def segment_yen(self):
        self.last_segment = seg.yen(self.last_frame)[1]
        return self.last_segment

    def segment_canny(self):
        print "segment canny"
        t1, t2 = int(self.cfg.get('segmentation', 'canny_t1')), int(self.cfg.get('segmentation', 'canny_t2'))
        self.last_segment = seg.canny(self.last_frame, t1, t2)
        return self.last_segment

    @ensure_img
    @ensure_segment
    def segment_hough(self):
        h_md, h_p1, h_p2, h_minr, h_maxr = self.cfg.get('segmentation', 'h_mindist'), self.cfg.get('segmentation', 'h_param1'), self.cfg.get('segmentation', 'h_param2'), self.cfg.get('segmentation', 'h_minradius'), self.cfg.get('segmentation', 'h_maxradius')
        imc, num =seg.hough_circular(self.last_segment, int(h_md), int(h_p1), int(h_p2), int(h_minr), int(h_maxr))
        self.last_log = "Numero circulos: %s"%(num)
        return imc


    @ensure_img
    @ensure_segment
    def measure_particles(self):
        print "Measure particles"
        ma  = self.cfg.getint('measures', 'min_area')
        imc, m =  meas.measure_particles(self.last_frame, self.last_segment, ma)
        self.last_log = ''
        llog = ''
        for i, j in enumerate(m):
            llog += '<tr><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>'%(i+1,j[0],j[1],j[1],j[2],j[4],j[5],j[6],j[7])
        llog = '<table><caption>Medidas</caption><tbody><tr><td>Id</td><td>Area</td><td>Perimetro</td><td>Excentricidad</td><td>Circularidad</td><td>Diametro equiv.</td><td>Solidez</td><td>Orientacion</td><td>Relacion de aspecto</td></tr>'+llog+'</tbody></table>'
        self.last_log = llog
        return imc


    ''' PROPAGACION '''
    @ensure_img
    def fft_fourier(self):
        dft_mag= fourier.cv_dft(self.last_frame)[1]
        dft_mag = (dft_mag * 255).astype(np.uint8)
        return dft_mag

    @ensure_img
    @log_errors
    def hologram_propagation(self):
        print "Hologram propagation"
        zi, zf = float(self.cfg.getfloat('hologram', 'zi')), float(self.cfg.getfloat('hologram', 'zf'))
        step = float(self.cfg.getfloat('hologram', 'step'))
        method = {'convolution': hologram.holo_convacel, 'angular': hologram.holo_angular, 'juptner': hologram.holo_juptner}
        m = self.cfg.get('hologram', 'method')

        #zi =61
        #zf = 62
        r = method[m](self.last_frame, zi, zf, step)[1]
        print "Finish hologram propagation"
        return r

    def hologram_superres(self):
        num = self.cfg.getint('super', 'num')
        pstep = self.cfg.getfloat('super', 'piezo_step')
        
        pos_init = 40
        self.setpiezo(pos_init)
        time.sleep(0.4) # Wait
        pos = pos_init
        frms = []
        for i in range(num):
            frms.append(self.grab()) #Acquire img
            pos += pstep
            self.setpiezo()
        imres = superres.pocs(frms, 4)
        return imres

    ''' CONTROL '''
    def setled(self, value):
        print "SET LED to", value
        if self.sm is not None:
            self.sm.send_command(SerialMessageManager.SEND_COM_SETLED, value)

    def setpiezo(self, value):
        print "SET PIEZO to", value
        if self.sm is not None:
            self.sm.send_command(SerialMessageManager.SEND_COM_SETPIEZO, value)

    def setled_auto(self):
        ''' Setear la iluminacion LED tomando imagenes a varios niveles (100)
        y midiendo la desviacion estandar'''
        print "Ajuste de LED "

        v = 0
        vi = 0
        m = []
        for i in range(100):
            self.setled(i)
            self.grab()
            measure = self.last_frame.std()/128.0
            print "Image", i,"measure", measure
            if measure > v:
                vi = i
                v = measure
        print "Max std %s, level %s"%(v, vi)
        self.setled(vi)
        self.grab()
        return self.last_frame
