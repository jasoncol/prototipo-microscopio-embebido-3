import cv2
import rprop

''' Para medir descriptores de forma de las particulas '''
def measure_particles(im, imseg, minarea=10):
    regs = rprop.get_regions(imseg)
    imc = cv2.cvtColor(im, cv2.COLOR_GRAY2BGR)
    m = []
    c = 0
    for i, r in enumerate(regs):
        if r.area < minarea:
            continue
        c += 1
        cv2.putText(imc, str(c+1), (int(r.centroid[0]), int(r.centroid[1])), cv2.FONT_HERSHEY_COMPLEX, 0.6, (0, 255, 0))
        m.append([r.area, r.perimeter, r.eccentricity, r.circularity, r.equiv_diameter, r.solidity, r.orientation, r.aspect_ratio])
    print "NUMBER OF REGIONS", c
    return imc, m
