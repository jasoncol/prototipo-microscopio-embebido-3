import struct
import time
from uart import SerialUART
from threading import Thread

class SerialMessageManager(object):
    HEADER_DATA = "ji"
    FOOTER_DATA = "jf"
    END, ENDR = '\n', '\r'
    COMMAND_SET_PWM = "4"
    SEP = ','
    LIMIT = 100

    def __init__(self, pserial=None, sendbin=True, *args, **kwargs):
        self.pserial = pserial
        self.sendbin = sendbin
        pass

    def send_command(self, command, value=''):
        data = self.SEP.join([self.HEADER_DATA, command, str(value),self.FOOTER_DATA, self.END]) 
        chksum = self.generate_chksum(data)
        data += chksum + self.ENDR
        
        print "Command: ", command, "value", value
        print "data is:", repr(data)
        print "chksum is ", chksum
        print "len bytes:", len(data)
        data2send = data
        if self.sendbin:
            data2send = struct.pack('%sc'%(len(data)), *data)

        #TODO data limits
        try:
            self.pserial.write(data2send, end='')
        except:
            print "Error sending data to serial"

    def receive_data(self):
        print "Starting daemon to receive data"
        self.run_t = True
        self.tf = Thread(target=self._recv_data)
        self.tf.daemon = True
        self.tf.start()

    def _recv_data(self):
        import random
        while self.run_t:
              out = ''
              try:
                  while self.pserial.p.inWaiting() > 0:#read buffer until is empty
                      out += self.pserial.read(1)
              except Exception:
                  print "Error reading buffer"

              if len(out):
                  print "Readed ", out, "BYTES: ", len(out)
              self.last_output = out
              time.sleep(0.25) #Read buffer 4 times per second
        print "Reading buffer ended"


    def generate_chksum(self, data):
        cval = 0
        for c in [ord(x) for x in data]:#TODO do we need unicode?
            cval ^= c
            
        return str(255 - cval)

    def validate_chksum(self, data, chksum):
        pass   
    
    def __del__(self):
        print "Closing port connection"
        self.run_t = False
        if self.pserial.p.isOpen():
           self.pserial.close()

if __name__ == '__main__':
    p = SerialUART()
    p.open('/dev/ttyO5')
    s = SerialMessageManager(p, sendbin=False)
    s.receive_data()
    s.send_command(SerialMessageManager.COMMAND_SET_PWM, 1000)
    
    while True: #Main thread simmulation
        try:
            val =int(raw_input('> Insert value to send: '))
            s.send_command(SerialMessageManager.COMMAND_SET_PWM, val)
            time.sleep(1)
        except KeyboardInterrupt:
            print "error k"
            s.__del__()
            break
        except Exception:
            print "error"
            s.__del__()
            break
