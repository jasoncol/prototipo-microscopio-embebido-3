#include<iostream>
#include<opencv2/opencv.hpp>
#include "opencv2/highgui/highgui.hpp"
using namespace std;
using namespace cv;
 
int main()
{
   VideoCapture capture(0);
   capture.set(CV_CAP_PROP_FRAME_WIDTH,320);
   capture.set(CV_CAP_PROP_FRAME_HEIGHT, 240);
   capture.set(CV_CAP_PROP_FPS, 5);
   if(!capture.isOpened()){
		        cout << "Failed to connect to the camera." << endl;
		    }
   Mat frame;

   for(int i=0; i<=500; i++){
	    cout << "reading frame " <<i<<endl;
           capture.read(frame);
   //capture >> frame;
   if(frame.empty()){
          cout << "Failed to capture an image" << endl;
	            return -1;
	        }
		    		    
     imshow("test", frame);
     std::stringstream sstm;
     sstm << "ocv_grab/out_" << i << ".bmp";
     imwrite(sstm.str(), frame);
     waitKey(200);
   }
   return 0;
}
