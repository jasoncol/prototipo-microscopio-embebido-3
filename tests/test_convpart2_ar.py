# -*- coding: utf-8 -*-
import numpy as np
from numpy.fft  import fft2, fftshift, ifft2
import cv2
import matplotlib.pyplot as plt
import time

def holo_convacel(im):
    ''' Implementación algoritmo de reconstrucción de holograma por convolución estandar con método acelerado. Basado en ConvolucionParticulas2.m '''
    imd = im.astype(np.float64)
    m, n = im.shape
    i = 0 + 1j
    deltaX = 9 * 10 ** -6
    deltaY = 9 * 10 ** -6
    contar = 10 #Distancia de registro y reconstrucción
    lambda_ = 632 * 10 ** -9 #Longitud de onda
    
    start,  end = 0.1, 0.2
    ddv = np.arange(start, end, 0.005) if end > start else (start, )

    g0 = np.zeros(imd.shape, np.complex)
    #IMP1
    '''
    #Implementación 1 usando FORS anidados. Numpy es muy lento usando esta aproximación. Es necesario usar operaciones matriciales.

    
    for dd in ddv:
        for fila in xrange(m):
            for columna in xrange(n):
                fila2 = (fila + 1) - (m / 2.)
                columna2 = (columna + 1) - (n / 2.)
                g0[fila, columna] = (1 / (i * lambda_)) * np.exp((i * 2 * np.pi / lambda_) * np.sqrt(dd ** 2 + ((fila2) ** 2) * deltaX ** 2 + ((columna2) **2) * deltaY ** 2)) / np.sqrt(dd ** 2 + ((fila2) ** 2) * (deltaX ** 2) + ((columna2) ** 2) * deltaY ** 2)
    '''
    #IMP2
    #USANDO operaciones matriciales
    #dd = 0.1
    fila = np.linspace(0, m-1, m) + 1. - (m / 2.)
    columna = np.linspace(0, n - 1, n) + 1. - (n / 2.)
    columna2, fila2 = np.meshgrid(columna, fila)

    for dd in ddv:
        g0 = (1 / (i * lambda_)) * np.exp((i * 2 * np.pi / lambda_) * np.sqrt(dd ** 2 + ((fila2) ** 2) * deltaX ** 2 + ((columna2) **2) * deltaY ** 2)) / np.sqrt(dd ** 2 + ((fila2) ** 2) * (deltaX ** 2) + ((columna2) ** 2) * deltaY ** 2)
        
        #FFT
        g00 = fft2(g0)
        obj = fft2(imd)
        
        resulta = obj * g00
        resulta2 = fftshift(ifft2(resulta))
        rea = abs(resulta2)
        ig = plt.imshow(abs(rea).astype(np.float32))
        ig.set_cmap('gray')
        plt.show()
        raw_input()
        contar += 1
    
    #MATRIZ PROPAGACION
    matriz_progagacion = (-i * np.pi * lambda_) * (((fila2 / (m * deltaX)) ** 2) +  ((columna2 / (n * deltaY)) ** 2))
    
    
    #Ult calculo
    zprima = 0.10;
    gnueva = np.exp(zprima * matriz_progagacion)
    gnueva00 = gnueva
    obj2 = obj # es el mismo obj
    resulta10 = fftshift(obj2) * gnueva00
    resulta20 = ifft2(resulta10)

    ig = plt.imshow(abs(rea).astype(np.float32))
    ig.set_cmap('gray')
    plt.show()
    raw_input()
    rea = abs(resulta20).astype(np.float32)
    return rea

if __name__ == '__main__':
    im = cv2.imread('Montaje1Frances.bmp', 0)
    #im = cv2.resize(im, (640, 480), interpolation=cv2.INTER_CUBIC)
    k = time.time()
    holo_convacel(im)
    print "ET", time.time() - k
