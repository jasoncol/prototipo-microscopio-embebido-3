'''
Prueba de performance:
- Lectura de imagen de disco
- Escritura png
- Escritura bmp
- Rescalado
- Otsu 
- Canny

author: jjmc82@gmail.com
'''
import time
import cv2
from test_fourier import cv_dft, pad_fourier
from test_convpart2_ar import holo_convacel
from test_holo_juptner import holo_juptner
import fftw3
import numpy as np
from yen import yen

N = 10 # Number of times a function is executed

def perf_exec(f, v):
    print '--'
    a = 0
    r = [100000, 0]
    print "Execution of ", f.__name__
    for i in range(N):
        k = time.time()
        f(v)
        elp = time.time() - k
        if elp > r[1]:
            r[1] = elp
        elif elp < r[0]:
            r[0] = elp
        a += elp

    print "Average Time: %f milliseconds (Max:%f, Min:%f)"%(1000. * a/N, r[1]*1000., r[0]*1000.)

#Functions to test
def imread(v):
    im = cv2.imread(v)
    return True

def imwrite_png(im):
    cv2.imwrite('m2_write%s.png'%(time.time()), im)

def imwrite_bmp(im):
    cv2.imwrite('m2_write%s.bmp'%(time.time()), im)

def otsu(im):
    cv2.threshold(im, 0, 255, cv2.THRESH_OTSU)

def canny(im):
    cv2.Canny(im, 10, 40)

def resize_up(im):
    cv2.resize(im, (im.shape[1] * 2, im.shape[0] * 2))

def resize_down(im):
    cv2.resize(im, (im.shape[1] / 2,im.shape[0] / 2))

def fft_cv(im):
    cv_dft(im)

def fft_w3(im):
    pad = pad_fourier(im)
    inputa = pad.astype(complex)
    outputa = np.zeros_like(inputa)

    fft = fftw3.Plan(inputa,outputa, direction='forward', flags=['measure'])
    fft.execute()

def fft_numpy(im):
    pad = pad_fourier(im)
    return np.fft.fft2(pad)

def test(v):
    c = (abs(v) **v + 1)
    return c

def bottom_hat(im):
    th = cv2.morphologyEx(im, cv2.MORPH_BLACKHAT, np.ones((15, 15)), anchor=(-1, -1))

def hough_cv(im):
    HOUGH_GRADIENT = 3
    imt = cv2.threshold(im, 162, 255, cv2.THRESH_BINARY)[1]
    ht = cv2.HoughCircles(imt,HOUGH_GRADIENT, dp=2, minDist=30, param1=100,param2=50, minRadius=3, maxRadius=100)

def fft_shift(im_fft):
    np.fft.fftshift(im_fft)

if __name__ == "__main__":
    perf_exec(test, -1)
    test_im = 'Montaje1Frances.bmp'#'1_several.png'#test.jpg
    imo = cv2.imread(test_im, 0)
    sizes = (320,  640, 960, 1280)
    

    for s in sizes:
        print "SIZE ", (s, s * 3 / 4)
        im =cv2.resize(imo, (s, s * 3 / 4))
        iw = 'test_%s.png'%(s)
        #cv2.imwrite(iw, im)
        '''
        perf_exec(imread, iw)
        perf_exec(imwrite_bmp, im)
        perf_exec(imwrite_png, im)
        perf_exec(otsu, im)
        perf_exec(yen, im)
        perf_exec(canny, im)
        perf_exec(bottom_hat, im)
        perf_exec(resize_up, im)
        perf_exec(resize_down, im)
        perf_exec(fft_cv, im)
        perf_exec(fft_w3, im)
        perf_exec(fft_numpy, im)
        perf_exec(hough_cv, im)
        perf_exec(fft_shift, fft_numpy(im))
        '''
        #perf_exec(holo_convacel, im)
        perf_exec(holo_juptner, im)
