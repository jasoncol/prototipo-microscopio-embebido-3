# -*- coding: utf-8 -*-
import numpy as np
from numpy.fft  import fft2, fftshift, ifft2
import cv2
#import matplotlib.pyplot as plt
import time

def holo_juptner(im):
    ''' Recostrucción del holograma basado en el método de Juptner usando transformada de Fesnel. Basado en RecuAprox_01.m '''
    kt = time.time()
    lambda_ = 0.0006328
    dist = 75.0
    distph = 19.1
    sizeph = 0.025
    sample = 0.001
    sampleccd = 0.009
    window = 400
    window2 = 400
    k = 2 * np.pi / lambda_
    z = distph + dist

    #Spherical samples of  5 um
    holoaux = im.astype(np.float64)
    cy, cx = int(im.shape[0] / 2), int(im.shape[1] / 2)
    holo2 = holoaux[round(cy - window / 2):round(cy + window/2 - 0) ,\
            round(cx - window/2): round(cx + window/2 - 0) ]
    holo3 = centrado_mat(np.zeros((window2, window2), np.float64), holo2, 'fusion')

    factor = sampleccd / sample
    holo = cv2.resize(holo3, (int(holo3.shape[0]*factor), int(holo3.shape[1]*factor)), interpolation=cv2.INTER_NEAREST)#NEAREST interp


    #Spherical wave phase
    space = len(holo[0])
    limite = sample * space / 2
    print "len", len(np.arange(-limite, limite-sample + sample , sample))
    n1 = np.arange(-limite, limite-sample + sample , sample)
    n2 = np.arange(-limite, limite-sample + sample, sample)
    x, y = np.meshgrid(n1,n2)
    
    phi, r = cart2polar(x, y)
    sphere_f = np.exp(1j * k *z) * np.exp(1j *k *r ** 2 / 2 / z)
    #Discrete Fresnel equation
    step = 1
    fromp = 71 # Initial distance from Hologram plane
    top = 71 # Final distance from hologram plane
    angle = 0.0
    rm = cv2.getRotationMatrix2D((cx, cy), angle, 1.)

    for jump in np.arange(fromp, top + step, step):
        dist2 = jump
        if (sample ** 2 * space / lambda_ / dist2) > 1:
            print "The phase is not being resolved in the spherical term"
            print "Reduce matrix size (space). reduce sample size (sample)"
            print "Increase wavelength (lambda), or increase the propagation distance (dist)"
        fr_sphere = np.exp(-1j * np.pi / lambda_ / dist2 * r ** 2)
        input_ = holo * sphere_f * fr_sphere
        sample_p = fftshift(ifft2(input_))
        isample_p = abs(sample_p * np.conj(sample_p)).astype(np.float64)
        rm = cv2.getRotationMatrix2D((isample_p.shape[1] / 2, isample_p.shape[0] / 2), angle, 1.)
        isample_p = cv2.warpAffine(isample_p, rm, (isample_p.shape[1], isample_p.shape[0]))

        zoom = round(space * (z - jump) / z / (jump ** .6))
        print "zoom", zoom
        imprint = centrado_mat(isample_p, np.zeros((zoom, zoom), np.float64), 'tajada')
        print imprint.max(), imprint.min(), imprint.mean()

        imprint = (imprint * 255./imprint.max()).astype(np.uint8)

        #cv2.imshow("test", imprint)
        #cv2.waitKey(200)

def centrado_mat(m1, m2, metodo='fusion'):
    ''' Centro de matriz M2 en la matriz M1. Basado en centradomat.m '''

    m, n = m1.shape
    mm, nn = m2.shape
    mat = m1.copy()

    if metodo == 'fusion':
        mat[round(m/2-mm/2) + 0:round(m/2+mm/2), round(n/2-nn/2)+0:round(n/2+nn/2)] = m2
    elif metodo == 'tajada':
        mat = m1[round(m/2-mm/2) + 1:round(m/2+mm/2), round(n/2-nn/2)+1:round(n/2+nn/2)]
    else:
        raise Exception #undefined method

    return mat

def cart2polar(x, y):
        r = np.sqrt(x**2 + y**2)
        theta = np.arctan2(y, x)
        return  theta, r

if __name__ == '__main__':
    im = cv2.imread('Montaje1Frances.bmp', 0)
    k = time.time()
    holo_juptner(im)
    print "ET", time.time() - k
